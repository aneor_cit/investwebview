package br.com.investimentos.investimentoswebview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class WebViewActivity extends AppCompatActivity {

    private WebView investimentosWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        investimentosWebView = (WebView) findViewById(R.id.activity_main_webview);

        // Enable Javascript
        WebSettings webSettings = investimentosWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        //// load webview
        investimentosWebView.loadUrl("http://172.16.4.166:3000/aporte-previdencia");
    }

    @Override
    public void onBackPressed() {
        if(investimentosWebView.canGoBack()) {
            investimentosWebView.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
